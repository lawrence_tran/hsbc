//
//  HSBCUITests.swift
//  HSBCUITests
//
//  Created by Lawrence Tran on 5/25/19.
//  Copyright © 2019 Lawrence Tran. All rights reserved.
//

import XCTest
@testable import HSBC

class HSBCUITests: XCTestCase {
    var app: XCUIApplication!
    
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app = XCUIApplication()
    }

    func testScrolling() {
        app.launch()
        
        let mainVC = app.otherElements["MainVC"]
        XCTAssertTrue(mainVC.exists, "MainVC not displaying")
        
        let tableQuery = app.tables
        XCTAssertNotNil(tableQuery.cells.matching(identifier: "ObjectiveCell").staticTexts["objective Value"], "Objective cell is not on screen")
        app.swipeUp()
        XCTAssertNotNil(tableQuery.cells.matching(identifier: "EducationCell2").staticTexts["Course 2"], "Education cell is not on screen")
        app.swipeDown()        
    }
    
}
