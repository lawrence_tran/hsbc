//
//  MockResumeNetwork.swift
//  HSBCTests
//
//  Created by Lawrence Tran on 5/25/19.
//  Copyright © 2019 Lawrence Tran. All rights reserved.
//

import Foundation
@testable import HSBC

class MockResumeNetwork: ResumeNetwork {
    
    static let instance: MockResumeNetwork = MockResumeNetwork()
    
    override class func sharedInstance() -> MockResumeNetwork {
        return SingletonManager.sharedInstance.getSingleton(MockResumeNetwork.self) as MockResumeNetwork
    }
    
    override class var managedSharedInstance: MockResumeNetwork {
        return instance
    }
    
    override func getResumeData(_ url: String, completion: @escaping (_ errorMessage: String?, _ data: Data?) -> Void) {
        guard let path = Bundle(for: self.classForCoder).path(forResource: "resume", ofType: "json") else {
            fatalError("The test data is missing from the bundle")
        }
        
        do {
            let resumeData = try Data(contentsOf: URL(fileURLWithPath: path), options: Data.ReadingOptions.mappedIfSafe)
            
            completion(nil, resumeData)
        } catch {
            // handle error
        }
    }
    
    func parseResumeData(_ data: Data?) -> ResumeModel? {
        do {
            guard let resumeData = data else {
                print("No Data")
                return nil
            }
            
            return try JSONDecoder().decode(ResumeModel.self, from: resumeData)
        } catch {
            return nil
        }
    }
}
