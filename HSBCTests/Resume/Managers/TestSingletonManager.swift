//
//  TestSingletonManager.swift
//  HSBCTests
//
//  Created by Lawrence Tran on 5/25/19.
//  Copyright © 2019 Lawrence Tran. All rights reserved.
//

import Foundation
@testable import HSBC

class TestSingletonManager: SingletonManager {
    static let instance = TestSingletonManager()
    
    override class func getSingletonManager() -> SingletonManager {
        return instance
    }
    
    /// <#Description#>
    ///
    /// - Parameter singletonType: <#singletonType description#>
    /// - Returns: <#return value description#>
    override func getSingleton<T: SingletonProtocol>(_ singletonType: T.Type) -> T {
        switch singletonType {
        case is ResumeNetwork.Type:
            return super.getSingleton(MockResumeNetwork.self) as! T
        default:
            return registerSingleton(singletonType)
        }
    }
}
