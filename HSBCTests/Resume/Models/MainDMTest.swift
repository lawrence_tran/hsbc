//
//  MainDMTest.swift
//  HSBCTests
//
//  Created by Lawrence Tran on 5/25/19.
//  Copyright © 2019 Lawrence Tran. All rights reserved.
//

import XCTest
@testable import HSBC

class MainDMTest: XCTestCase {
    func testSectionEnum() {
        XCTAssert(MainDM.Sections.allCases.count == 5, "Section Count has changed")
        
        XCTAssert(MainDM.Sections(rawValue: 0) == MainDM.Sections.Objective, "Sections have changed")
        XCTAssert(MainDM.Sections(rawValue: 1) == MainDM.Sections.Skills, "Sections have changed")
        XCTAssert(MainDM.Sections(rawValue: 2) == MainDM.Sections.Experience, "Sections have changed")
        XCTAssert(MainDM.Sections(rawValue: 3) == MainDM.Sections.Project, "Sections have changed")
        XCTAssert(MainDM.Sections(rawValue: 4) == MainDM.Sections.Education, "Sections have changed")
        
        XCTAssertEqual(MainDM.Sections(rawValue: 0)?.sectionName(), "Objective", "Section Names have changed")
        XCTAssertEqual(MainDM.Sections(rawValue: 1)?.sectionName(), "Skills", "Section Names have changed")
        XCTAssertEqual(MainDM.Sections(rawValue: 2)?.sectionName(), "Experience", "Section Names have changed")
        XCTAssertEqual(MainDM.Sections(rawValue: 3)?.sectionName(), "Project", "Section Names have changed")
        XCTAssertEqual(MainDM.Sections(rawValue: 4)?.sectionName(), "Education", "Section Names have changed")
    }
    
    func testNumberOfSection() {
        ResumeNetwork.sharedInstance().getResumeData("") { (error, data) in
            guard let resume = MockResumeNetwork().parseResumeData(data) else {
                return
            }
            let dm = MainDM(dm: resume)
            
            XCTAssert(dm.numberOfSections() == 5, "Section Count has changed")
        }
    }
    
    func testNumberOfRowsInSection() {
        ResumeNetwork.sharedInstance().getResumeData("") { (error, data) in
            guard let resume = MockResumeNetwork().parseResumeData(data) else {
                return
            }
            let dm = MainDM(dm: resume)
            
            XCTAssert(dm.numberOfRowsInSection(0) == 1, "Rows in sections has changed")
            XCTAssert(dm.numberOfRowsInSection(1) == 1, "Rows in sections has changed")
            XCTAssert(dm.numberOfRowsInSection(2) == 2, "Rows in sections has changed")
            XCTAssert(dm.numberOfRowsInSection(3) == 2, "Rows in sections has changed")
            XCTAssert(dm.numberOfRowsInSection(4) == 2, "Rows in sections has changed")
            
            // Bad Data
            XCTAssert(dm.numberOfRowsInSection(5) == 0, "Bad data accepted")
        }
    }
    
    func testTextForSection() {
        ResumeNetwork.sharedInstance().getResumeData("") { (error, data) in
            guard let resume = MockResumeNetwork().parseResumeData(data) else {
                return
            }
            let dm = MainDM(dm: resume)
            
            XCTAssertEqual(dm.textForSection(0), "Objective", "Section Names have changed")
            XCTAssertEqual(dm.textForSection(1), "Skills", "Section Names have changed")
            XCTAssertEqual(dm.textForSection(2), "Experience", "Section Names have changed")
            XCTAssertEqual(dm.textForSection(3), "Project", "Section Names have changed")
            XCTAssertEqual(dm.textForSection(4), "Education", "Section Names have changed")
            
            // Bad Data
            XCTAssertEqual(dm.textForSection(5), "", "Bad data accepted")
        }
    }
    
    func testDataForRowInSection() {
        ResumeNetwork.sharedInstance().getResumeData("") { (error, data) in
            guard let resume = MockResumeNetwork().parseResumeData(data) else {
                return
            }
            let dm = MainDM(dm: resume)
            
            XCTAssertNotNil(dm.dataForRowInSection(section: 0, row: 0) as? String, "Data Type is wrong")
            XCTAssertNotNil(dm.dataForRowInSection(section: 1, row: 0) as? Skills, "Data Type is wrong")
            XCTAssertNotNil(dm.dataForRowInSection(section: 2, row: 0) as? Experience, "Data Type is wrong")
            XCTAssertNotNil(dm.dataForRowInSection(section: 3, row: 0) as? Project, "Data Type is wrong")
            XCTAssertNotNil(dm.dataForRowInSection(section: 4, row: 0) as? Experience, "Data Type is wrong")
            
            XCTAssertNil(dm.dataForRowInSection(section: 4, row: 3) as? Experience, "Bad data accepted")
            XCTAssertNil(dm.dataForRowInSection(section: 5, row: 0) as? String, "Bad data accepted")
        }
    }
}
