# HSBC Test

I have included my resume as part of the repo for any one that requires it.

The json data used for the app is hosted at
"https://gist.githubusercontent.com/Ludicrous2k/05f2d208647a1f58b03292b577b241fd/raw/e0e1a67108ec986c0154d91fd634aca818fc2cec/resume.json""

The json is also available in the repo for reviewing if required. It's located in with the unit test.
