//
//  MainVM.swift
//  HSBC
//
//  Created by Lawrence Tran on 5/25/19.
//  Copyright © 2019 Lawrence Tran. All rights reserved.
//

import Foundation
import UIKit

protocol MainVMDelegate {
    func reloadTable()
}

class MainVM {
    var dm: MainDM?
    let urlString = "https://gist.githubusercontent.com/Ludicrous2k/05f2d208647a1f58b03292b577b241fd/raw/abc95488a998478fd9a7d1388c54653ac75a86aa/resume.json" // normally read in from a remote config or api call
    let delegate: MainVMDelegate
    
    init(_ delegate: MainVMDelegate) {
        self.delegate = delegate
        getResumeData()
    }
    
    // Mark: - UITableView
    func numberOfSections() -> Int {
        return dm?.numberOfSections() ?? 0
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return dm?.numberOfRowsInSection(section) ?? 0
    }
    
    func titleForHeaderInSection(_ section: Int) -> String? {
        return dm?.textForSection(section) ?? ""
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let sectionEnum = MainDM.Sections(rawValue: indexPath.section) else {
            return UITableViewCell()
        }
        
        let cellIdentifier = "\(sectionEnum.sectionName().lowercased())Cell"
        var cell: UITableViewCell
        
        let section = indexPath.section
        let row = indexPath.row
        
        switch sectionEnum {
        case .Objective:
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
            if let objective = dm?.dataForRowInSection(section: section, row: row) as? String {
                cell.textLabel?.text = objective
            }
            cell.accessibilityLabel = "\(sectionEnum)Cell"
            cell.accessibilityIdentifier = "\(sectionEnum)Cell"
            cell.selectionStyle = .none
        case .Skills:
            let skillCell = SkillsTableViewCell.createCell(tableView)
            if let skills = dm?.dataForRowInSection(section: section, row: row) as? Skills {
                skillCell.configureCell(skills)
            }
            skillCell.setAccessibilityId(cellIdentifier)
            cell = skillCell
        case .Education:
            fallthrough
        case .Experience:
            let expCell = ExperienceTableViewCell.createCell(tableView)
            if let exp = dm?.dataForRowInSection(section: section, row: row) as? Experience {
                expCell.configureCell(exp)
            }
            expCell.setAccessibilityId("\(cellIdentifier)\(row)")
            cell = expCell
        case .Project:
            let projectCell = ProjectTableViewCell.createCell(tableView)
            if let project = dm?.dataForRowInSection(section: section, row: row) as? Project {
                projectCell.configureCell(project)
            }
            projectCell.setAccessibilityId("\(cellIdentifier)\(row)")
            cell = projectCell
        }
        
        return cell
    }
    
    // Mark: - Network
    private func getResumeData() {
        ResumeNetwork.sharedInstance().getResumeData(urlString) { [weak self] (error, data) in
            do {
                guard let resumeData = data else {
                    // handle error
                    if let error = error {
                        print(error)
                    }
                    
                    print("No Data")
                    return
                }
                
                let resume = try JSONDecoder().decode(ResumeModel.self, from: resumeData)
                self?.dm = MainDM(dm: resume)
                
                DispatchQueue.main.async {
                    self?.delegate.reloadTable()
                }
            } catch {
                // handle error
            }
        }
    }
}
