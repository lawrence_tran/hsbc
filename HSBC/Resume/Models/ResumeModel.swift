//
//  ResumeModel.swift
//  HSBC
//
//  Created by Lawrence Tran on 5/25/19.
//  Copyright © 2019 Lawrence Tran. All rights reserved.
//

import Foundation

/// <#Description#>
struct ResumeModel: Decodable {
    
    /// <#Description#>
    let objective: String
    
    /// <#Description#>
    let skills: Skills
    
    /// <#Description#>
    let experience: [Experience]
    
    /// <#Description#>
    let project: [Project]
    
    /// <#Description#>
    let education: [Experience]
}

/// <#Description#>
struct Skills: Decodable {
    
    /// <#Description#>
    let teamwork: [String]
    
    /// <#Description#>
    let additional: [String]
}

/// <#Description#>
struct Experience: Decodable {
    
    /// <#Description#>
    let title: String
    
    /// <#Description#>Ø
    let companyName: String
    
    /// <#Description#>
    let startYear: String
    
    /// <#Description#>
    let endYear: String
    
    /// <#Description#>
    let notes: [String]?
    
    private enum CodingKeys: String, CodingKey {
        case title
        case companyName
        case startYear
        case endYear
        case notes = "additional"
    }
}

/// <#Description#>
struct Project: Decodable {
    
    /// <#Description#>
    let projectName: String
    
    /// <#Description#>
    let companyName: String
    
    /// <#Description#>
    let projectSummary: String
}
