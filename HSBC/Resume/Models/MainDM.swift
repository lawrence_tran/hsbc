//
//  MainDM.swift
//  HSBC
//
//  Created by Lawrence Tran on 5/25/19.
//  Copyright © 2019 Lawrence Tran. All rights reserved.
//

import Foundation

struct MainDM {
    var resume: ResumeModel
    
    enum Sections: Int, CaseIterable {
        case Objective
        case Skills
        case Experience
        case Project
        case Education
        
        /// <#Description#>
        ///
        /// - Returns: <#return value description#>
        func sectionName() -> String {
            switch self {
            case .Objective:
                return "Objective"
            case .Skills:
                return "Skills"
            case .Experience:
                return "Experience"
            case .Project:
                return "Project"
            case .Education:
                return "Education"
            }
        }
    }
    
    init(dm: ResumeModel) {
        self.resume = dm
    }
    
    /// <#Description#>
    ///
    /// - Returns: <#return value description#>
    func numberOfSections() -> Int {
        return Sections.allCases.count
    }
    
    /// <#Description#>
    ///
    /// - Parameter section: <#section description#>
    /// - Returns: <#return value description#>
    func numberOfRowsInSection(_ section: Int) -> Int {
        guard let sectionValue = Sections(rawValue: section) else {
            return 0
        }
        
        switch sectionValue {
        case .Objective:
            fallthrough
        case .Skills:
            return 1
        case .Experience:
            return resume.experience.count
        case .Project:
            return resume.project.count
        case .Education:
            return resume.education.count
        }
    }
    
    /// <#Description#>
    ///
    /// - Parameter section: <#section description#>
    /// - Returns: <#return value description#>
    func textForSection(_ section: Int) -> String {
        return Sections(rawValue: section)?.sectionName() ?? ""
    }
    
    /// <#Description#>
    ///
    /// - Parameters:
    ///   - section: <#section description#>
    ///   - row: <#row description#>
    /// - Returns: <#return value description#>
    func dataForRowInSection(section: Int, row: Int) -> Any? {
        guard let sectionValue = Sections(rawValue: section) else {
            return nil
        }
        
        switch sectionValue {
        case .Objective:
            return resume.objective
        case .Skills:
            return resume.skills
        case .Experience:
            if row <= resume.experience.count {
                return resume.experience[row]
            }
        case .Project:
            if row <= resume.project.count {
                return resume.project[row]
            }
        case .Education:
            if row <= resume.education.count {
                return resume.education[row]
            }
        }
        
        return nil
    }
}
