//
//  SkillsTableViewCell.swift
//  HSBC
//
//  Created by Lawrence Tran on 5/25/19.
//  Copyright © 2019 Lawrence Tran. All rights reserved.
//

import UIKit

class SkillsTableViewCell: UITableViewCell, TableCellProtocol {

    @IBOutlet weak var teamworkTitleLabel: UILabel!
    @IBOutlet weak var teamworkTextLabel: UILabel!
    @IBOutlet weak var additionalSkillTitleLabel: UILabel!
    @IBOutlet weak var additionalSkillTextLabel: UILabel!
    
    static func createCell(_ tableView: UITableView) -> SkillsTableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "skillsCell") as? SkillsTableViewCell else {
            fatalError("Can't find SkillsTableViewCell")
        }
        
        return cell
    }
    
    func configureCell(_ dm: Skills) {
        clearContent()
        
        teamworkTitleLabel.text = "Teamwork Skills" // would use localized file normally
        teamworkTextLabel.text = dm.teamwork.joined(separator: "\n")
        additionalSkillTitleLabel.text = "Additional Skill" // would use localized file normally
        additionalSkillTextLabel.text = dm.additional.joined(separator: "\n")
        
        teamworkTextLabel.sizeToFit()
        additionalSkillTextLabel.sizeToFit()
    }
    
    func clearContent() {
        teamworkTitleLabel.text = ""
        teamworkTextLabel.text = ""
        additionalSkillTitleLabel.text = ""
        additionalSkillTextLabel.text = ""
    }
    
    func setAccessibilityId(_ accessibilityId: String) {
        accessibilityIdentifier = accessibilityId
    }
}
