//
//  ProjectTableViewCell.swift
//  HSBC
//
//  Created by Lawrence Tran on 5/25/19.
//  Copyright © 2019 Lawrence Tran. All rights reserved.
//

import UIKit

class ProjectTableViewCell: UITableViewCell, TableCellProtocol {

    @IBOutlet weak var projectNameLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var projectSummaryLabel: UILabel!
    
    static func createCell(_ tableView: UITableView) -> ProjectTableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "projectCell") as? ProjectTableViewCell else {
            fatalError("Can't find ProjectTableViewCell")
        }
        
        return cell
    }
    
    func configureCell(_ dm: Project) {
        clearContent()
        
        projectNameLabel.text = dm.projectName
        companyNameLabel.text = dm.companyName
        projectSummaryLabel.text = dm.projectSummary
        
        projectSummaryLabel.sizeToFit()
    }
    
    func clearContent() {
        projectNameLabel.text = ""
        companyNameLabel.text = ""
        projectSummaryLabel.text = ""
    }
    
    func setAccessibilityId(_ accessibilityId: String) {
        accessibilityIdentifier = accessibilityId
    }
}
