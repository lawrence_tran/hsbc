//
//  ExperienceTableViewCell.swift
//  HSBC
//
//  Created by Lawrence Tran on 5/25/19.
//  Copyright © 2019 Lawrence Tran. All rights reserved.
//

import UIKit

class ExperienceTableViewCell: UITableViewCell, TableCellProtocol {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var additionalTextLabel: UILabel!
    
    static func createCell(_ tableView: UITableView) -> ExperienceTableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "experienceCell") as? ExperienceTableViewCell else {
            fatalError("Can't find ExperienceTableViewCell")
        }
        
        return cell
    }
    
    func configureCell(_ dm: Experience) {
        clearContent()
        
        titleLabel.text = dm.title
        companyLabel.text = dm.companyName
        yearLabel.text = "\(dm.startYear) - \(dm.endYear)"
        additionalTextLabel.text = dm.notes?.joined(separator: "\n")
        
        additionalTextLabel.sizeToFit()
    }
    
    func clearContent() {
        titleLabel.text = ""
        companyLabel.text = ""
        yearLabel.text = ""
        additionalTextLabel.text = ""
    }
    
    func setAccessibilityId(_ accessibilityId: String) {
        accessibilityIdentifier = accessibilityId
    }
}
