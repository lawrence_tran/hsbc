//
//  MainVC.swift
//  HSBC
//
//  Created by Lawrence Tran on 5/25/19.
//  Copyright © 2019 Lawrence Tran. All rights reserved.
//

import UIKit

class MainVC: UIViewController, UITableViewDelegate, UITableViewDataSource, MainVMDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var vm: MainVM?
    
    // Mark: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.vm = MainVM(self)
    }
    
    // Mark: - UITableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return vm?.numberOfSections() ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm?.numberOfRowsInSection(section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return vm?.titleForHeaderInSection(section) ?? ""
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return vm?.tableView(tableView, cellForRowAt: indexPath) ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // Mark: - MainVMDelegate
    func reloadTable() {
        tableView.reloadData()
    }
}

