//
//  ResumeNetwork.swift
//  HSBC
//
//  Created by Lawrence Tran on 5/25/19.
//  Copyright © 2019 Lawrence Tran. All rights reserved.
//

import Foundation

class ResumeNetwork: NSObject, SingletonProtocol {
    static private let instance: ResumeNetwork = ResumeNetwork()
    
    class var managedSharedInstance: ResumeNetwork {
        return instance
    }
    
    class func sharedInstance() -> ResumeNetwork {
        return SingletonManager.sharedInstance.getSingleton(ResumeNetwork.self)
    }
    
    func getResumeData(_ url: String, completion: @escaping (_ errorMessage: String?, _ data: Data?) -> Void) {
        guard let url = URL(string: url) else {
            // handle error
            return
        }

        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                completion(error.localizedDescription, data)
                return
            }
            
            completion(error?.localizedDescription, data)
        }.resume()
    }
}
