//
//  SingletonManager.swift
//  HSBC
//
//  Created by Lawrence Tran on 5/25/19.
//  Copyright © 2019 Lawrence Tran. All rights reserved.
//

import Foundation

/// <#Description#>
class SingletonManager: NSObject {
    static let sharedInstance = SingletonManager.getSingletonManager()
    private var singletonArray = [String: Any]()
    
    /// <#Description#>
    ///
    /// - Returns: <#return value description#>
    class func getSingletonManager() -> SingletonManager {
        if SingletonManager.isTesting() {
            if let testSingletonsType = NSClassFromString("HSBCTests.TestSingletonManager") as? NSObject.Type {
                let testSingletons = testSingletonsType.init()
                return testSingletons as? SingletonManager ?? SingletonManager()
            }
        }
        
        return SingletonManager()
    }
    
    /// <#Description#>
    ///
    /// - Returns: <#return value description#>
    private class func isTesting() -> Bool {
        return NSClassFromString("XCTest") != nil ? true : false
    }
    
    /// <#Description#>
    ///
    /// - Parameter singletonType: <#singletonType description#>
    /// - Returns: <#return value description#>
    func getSingleton<T: SingletonProtocol>(_ singletonType: T.Type) -> T {
        if let singleton = singletonArray[String(describing: singletonType)] {
            return singleton as! T
        }
        return registerSingleton(singletonType)
    }
    
    /// <#Description#>
    ///
    /// - Parameter singletonType: <#singletonType description#>
    /// - Returns: <#return value description#>
    func registerSingleton<T: SingletonProtocol>(_ singletonType: T.Type) -> T {
        let instance = singletonType.managedSharedInstance
        singletonArray[String(describing: singletonType)] = instance as Any?
        return instance as! T
    }
}
