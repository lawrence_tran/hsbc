//
//  TableCellProtocol.swift
//  HSBC
//
//  Created by Lawrence Tran on 5/25/19.
//  Copyright © 2019 Lawrence Tran. All rights reserved.
//

import Foundation
import UIKit

protocol TableCellProtocol {
    associatedtype DataModel
    associatedtype Cell
    
    static func createCell(_ tableView: UITableView) -> Cell
    func configureCell(_ dm: DataModel)
    func clearContent()
    func setAccessibilityId(_ accessibilityId: String)
}
