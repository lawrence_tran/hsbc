//
//  SingletonProtocol.swift
//  HSBC
//
//  Created by Lawrence Tran on 5/25/19.
//  Copyright © 2019 Lawrence Tran. All rights reserved.
//

import Foundation

protocol SingletonProtocol: class {
    associatedtype Manager
    
    static var managedSharedInstance: Manager { get }
    static func sharedInstance() -> Manager
}
